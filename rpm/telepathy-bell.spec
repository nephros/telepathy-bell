Name:       telepathy-bell

%define keepstatic 1

Summary:    Connection Manager for Jami (GNU Ring)
Version:    1.0
Release:    0
License:    GPLv3
URL:        https://github.com/alok4nand/telepathy-bell
Source0:    %{name}-%{version}.tar.bz2
Source1:    %{name}.privileges
Source31:    jami.provider
Source32:    jami.service
# accounts plugin
Source33:    jami.qml
Source34:    JamiCommon.qml
Source35:    JamiSettingsDisplay.qml
Source36:    jami-settings.qml
Source37:    jami-update.qml
Source50:    https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/resources/images/jami.svg
#Patch0:     0001-python3-changes.patch
Patch0:     bootstrap-server.patch
Requires:   mapplauncherd
BuildRequires:  cmake
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(check)
BuildRequires:  pkgconfig(libxslt)
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(dbus-glib-1)
BuildRequires:  pkgconfig(uuid)
BuildRequires:  pkgconfig(telepathy-glib) >= 0.11.7
#BuildRequires:  pkgconfig(mission-control-plugins)
BuildRequires:  pkgconfig(libngf0) >= 0.24
BuildRequires:  pkgconfig(libdbusaccess)
BuildRequires:  pkgconfig(python3)
BuildRequires:  pkgconfig(systemd)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5DBus)
BuildRequires:  pkgconfig(Qt5Xml)
BuildRequires:  pkgconfig(Qt5Network)
#BuildRequires:  pkgconfig(Qt5Gui)
#BuildRequires:  pkgconfig(Qt5Quick)
#BuildRequires:  pkgconfig(Qt5QmlDevTools)
#BuildRequires:  pkgconfig(Qt5QmlDevTools)
BuildRequires:  qtchooser
BuildRequires:  qt5-qmake
BuildRequires:  telepathy-qt5-devel
BuildRequires:  telepathy-qt5-farstream-devel
BuildRequires:  sailfish-svg2png

%description
%{summary}.

%package tests
Summary:    Tests for %{name}
Requires:   %{name} = %{version}-%{release}

%description tests
%{summary}.

%package devel
Summary:    Development files for %{name}
Requires:   %{name} = %{version}-%{release}

%description devel
%{summary}.

%package doc
Summary:   Documentation for %{name}
Requires:  %{name} = %{version}-%{release}

%description doc
Man page for %{name}.

%package -n jolla-settings-accounts-extensions-jami
Summary:    Extension plugin for Jami/Ring accounts

%description -n jolla-settings-accounts-extensions-jami
%{summary}.

%prep
%autosetup -p1 -n %{name}-%{version}/upstream

%build
#mkdir m4 || true
#
#%%reconfigure 
%cmake .

make %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install
install -D -m0644 %{SOURCE1} \
        %{buildroot}%{_datadir}/mapplauncherd/privileges.d/%{name}.privileges
# accounts plugin
install -D -m0644 %{SOURCE31} \
        %{buildroot}%{_datadir}/accounts/providers/jami.provider
install -D -m0644 %{SOURCE32} \
        %{buildroot}%{_datadir}/accounts/services/jami.service
install -D -m0644 %{SOURCE33} \
        %{buildroot}%{_datadir}/accounts/ui/jami.qml
install -D -m0644 %{SOURCE34} \
        %{buildroot}%{_datadir}/accounts/ui/JamiCommon.qml
install -D -m0644 %{SOURCE35} \
        %{buildroot}%{_datadir}/accounts/ui/JamiSettingsDisplay.qml
install -D -m0644 %{SOURCE36} \
        %{buildroot}%{_datadir}/accounts/ui/jami-settings.qml
install -D -m0644 %{SOURCE37} \
        %{buildroot}%{_datadir}/accounts/ui/jami-update.qml
# some graphics
install -D -m0644 %{SOURCE50} \
        %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/jami.svg
install -D -m0644 %{SOURCE50} \
 %{_tmppath}/icon-m-service-jami.svg
for size in 1.0 1.25 2.0; do
mkdir -p %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z${size}/
sailfish_svg2png -z ${size} %{_tmppath} %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z${size}/icons
done
mkdir -p %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.5/icons
mkdir -p %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.5-large/icons
mkdir -p %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.75/icons
sailfish_svg2png -z 1.5 -s 36 48 60 72 108 144 128 %{_tmppath} %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.5-large/icons
sailfish_svg2png -z 1.5 -s 36 48 72 96 144 192 150 %{_tmppath} %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.5/icons
sailfish_svg2png -z 1.75 -w 1080 %{_tmppath} %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.75/icons
rm -f %{_tmppath}/icon-m-service-jami.svg
install -D -m0644 %{SOURCE50} \
 %{_tmppath}/graphic-service-jami.svg
sailfish_svg2png -z 1.75 -w 1080 %{_tmppath} %{buildroot}%{_datadir}/themes/sailfish-default/meegotouch/z1.75/icons


%post -n jolla-settings-accounts-extensions-jami
/usr/libexec/manage-groups add account-jami || :

%postun -n jolla-settings-accounts-extensions-jami
if [ "$1" -eq 0 ]; then
    /usr/libexec/manage-groups remove account-jami || :
fi

%files
%defattr(-,root,root,-)
%license COPYING
#%%{_userunitdir}/*
%{_datadir}/dbus-1/services/*
%{_datadir}/telepathy/managers/*
%{_datadir}/icons/*/scalable/*/*.svg
%{_libexecdir}/*
#%%{_libdir}/mission-control-plugins.0/mcp-account-manager-ring.so
%{_datadir}/mapplauncherd/privileges.d/%{name}.privileges

#%%files tests
#%%defattr(-,root,root,-)
#%/opt/tests/%%{name}/*

#%%files devel
#%%defattr(-,root,root,-)
#%%{_libdir}/*.a
#%%{_includedir}/*

#%%files doc
#%%defattr(-,root,root,-)
#%%{_mandir}/man*/%%{name}.*

%files -n jolla-settings-accounts-extensions-jami
%{_datadir}/accounts/providers/jami.provider
%{_datadir}/accounts/services/jami.service
%{_datadir}/accounts/ui/*
%{_datadir}/themes/sailfish-default/meegotouch/*/*

