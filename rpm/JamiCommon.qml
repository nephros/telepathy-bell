import QtQuick 2.0
import Sailfish.Silica 1.0
import com.jolla.settings.accounts 1.0

Column {
    property bool editMode
    property bool ringidEdited
    property bool ringaccountidEdited
    property bool usernameEdited
    property bool passwordEdited
    property alias ringid: ringidField.text
    property alias accounttype: typeBox.typename
    property alias ringaccountid: ringidField.text
    property alias username: usernameField.text
    property alias password: passwordField.text
    property alias hostname: hostnameField.text
    property bool acceptableInput:
                    ringidField.acceptableInput
                    && usernameField.acceptableInput
                    && passwordField.acceptableInput

    signal acceptBlocked

    width: parent.width

    TextField {
        id: ringidField

        visible: !editMode
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase

        label: qsTr("Ring ID (leave empty to create new)")

        onTextChanged: {
            if (activeFocus) {
                ringidEdited = true
            }
        }
        onAcceptableInputChanged: if (acceptableInput) errorHighlight = false
        onActiveFocusChanged: if (!activeFocus) errorHighlight = !acceptableInput

        EnterKey.iconSource: "image://theme/icon-m-enter-next"
        EnterKey.onClicked: usernameField.focus = true
    }

    TextField {
        id: usernameField

        visible: !editMode
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase

        //: XMPP username
        //% "Username"
        label: qsTrId("components_accounts-la-jabber_username")

        onTextChanged: {
            if (activeFocus) {
                usernameEdited = true
                // Updating username also updates password; clear it if it's default value
                if (!passwordEdited) {
                    passwordField.text = ""
                }
            }
        }
        onAcceptableInputChanged: if (acceptableInput) errorHighlight = false
        onActiveFocusChanged: if (!activeFocus) errorHighlight = !acceptableInput

        EnterKey.iconSource: "image://theme/icon-m-enter-next"
        EnterKey.onClicked: passwordField.focus = true
    }

    PasswordField {
        id: passwordField

        visible: !editMode

        onActiveFocusChanged: if (!activeFocus) errorHighlight = !acceptableInput
        onAcceptableInputChanged: if (acceptableInput) errorHighlight = false

        onTextChanged: {
            if (activeFocus && !passwordEdited) {
                passwordEdited = true
            }
        }

        EnterKey.iconSource: "image://theme/icon-m-enter-next"
    }

    SectionHeader {
        //% "Advanced settings"
        text: qsTrId("components_accounts-la-jabber_advanced_settings_header")
    }

    ComboBox {
      id: typeBox
      property string typename: currentIndex != 0 ? "sip" : "Ring"
      label: qsTr("Account Type")
      currentIndex: 0
      menu: ContextMenu {
        MenuItem { text: "Ring" }
        MenuItem { text: "SIP (not supported)"; enabled: false }
      }
    }

    ComboBox {
      id: hostnameBox
      //: XMPP server address
      //% "Server address"
      label: qsTrId("components_accounts-la-jabber_server")
      currentIndex: 2
      menu: ContextMenu {
        MenuItem { text: "Custom..." }
        MenuItem { text: "localhost" }
        MenuItem { text: "bootstrap.jami.net" }
      }
      onCurrentIndexChanged: {
        hostnameField.text = (currentIndex > 0) ? currentItem.text : ""
      }
    }

    TextField {
        id: hostnameField
        readOnly: (hostnameBox.currentIndex != 0)
        visible: (hostnameBox.currentIndex == 0)
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
        //: XMPP server address
        //% "Server address"
        label: qsTrId("components_accounts-la-jabber_server")
        EnterKey.iconSource: "image://theme/icon-m-enter-next"
    }

    TextField {
        id: ringaccountidField

        visible: !editMode
        inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase

        label: qsTr("Ring Account ID")

        onTextChanged: {
            if (activeFocus) {
                ringaccountidEdited = true
            }
        }
        onAcceptableInputChanged: if (acceptableInput) errorHighlight = false
        onActiveFocusChanged: if (!activeFocus) errorHighlight = !acceptableInput

        EnterKey.iconSource: "image://theme/icon-m-enter-next"
    }


}
