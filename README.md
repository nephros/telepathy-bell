# Telepathy Bell


## Creating an account:


 1. install jami-daemon
 1. install telepathy-bell

    mc-tool add bell/Ring Jami string:Username=<<username>> string:account=<<name>>_ string:RingID=ring:
    mc-tool list

you should now see a new jami account in the list, with an ID added. We assume `jamitest_50` here.

    mc-tool update bell/Ring/jamitest_50 string:hostname=localhost
    mc-tool enable bell/Ring/jamitest_50
    mc-tool request bell/Ring/jamitest_50 online
    mc-tool reconnect bell/Ring/jamitest_50

check the status:

    mc-tool show bell/Ring/jamitest_50


